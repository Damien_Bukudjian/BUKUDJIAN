<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

function connexion(){
    $dsn='mysql:dbname=finder;host=127.0.0.1';
    $user='admin';
    $password='3xpl05!0';
    return $dbh=new PDO($dsn,$user,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getChambre($id)
{
    $sql = "SELECT * FROM chambre WHERE id=:id";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}
function getAccount($id, $nom)
{
    $sql = "SELECT * FROM accounts WHERE id=:id AND nom=:nom";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":nom", $nom);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

function insertUser($id, $nom)
{
    $sql = "INSERT INTO `accounts` (`id`, `email`, `password`, `nom`) VALUES (:id, :email, '', :nom)";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":nom", $nom);
        $statement->bindParam(":email", $email);
        $statement->execute();
        return "réussi";
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}
    
$app = new \Slim\App;
$app->get('/bonjour', function(Request $request, Response $response){  
  return "bonjour";
});
$app->get('/chambre/{id}', function(Request $request, Response $response){  
    $id = $request->getAttribute('id');
    return getChambre($id);
});
$app->get('/user', function(Request $request, Response $response){  
    $tb = $request->getQueryParams();
    $id = $tb["id"];
    $nom = $tb["nom"];
    return getAccount($id, $nom);
});
$app->post('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams(); 
    $id = $tb["id"];
    $nom = $tb["nom"];
    $email = $tb["email"];
    return insertUser($id, $nom, $email);
});
$app->run();