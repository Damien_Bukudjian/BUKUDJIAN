from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('chambres', views.chambres, name='chambres'),
    path('getchambres', views.getchambres, name='getchambres'),
]