from django.http import HttpResponse
from django.shortcuts import render
import json
import sqlite3 as sql

def index(request):
    return HttpResponse("Hello, waza")
    

def chambres(request):
    return render(request, 'finder/chambres.html')
    
def getchambres(request):
    con = sql.connect("sqlite-master/finder.db")
    con.row_factory = sql.Row
    cur = con.cursor()
    cur.execute("select * from chambre")
    data=[]
    rows = cur.fetchall(); 
    for row in rows:
        d = dict()
        d['id']=row[0]
        d['nbCouchage']=row[1]
        d['porte']=row[2]
        d['etage']=row[3]
        d['idCategorie']=row[4]
        d['baignoire']=row[5]
        d['prixBase']=row[6]
        data.append(d)
    if len(data) != 0:
        data=json.dumps(data)
        return HttpResponse(data,status=200,content_type="application/json")
    else:
        data= json.dumps({'error':str(data[0])})
        return HttpResponse(data,status=400,content_type="application/json")
    conn.close()