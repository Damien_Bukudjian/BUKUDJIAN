from flask import Flask
from flask_cors import CORS
from flask import request
from flask import jsonify
from datetime import datetime
import json
import sqlite3 as sql
import jwt
app = Flask(__name__)
CORS(app)

@app.route("/bonjour")
def hello():
  return "Bonjour" 

@app.route('/chambre/<id>', methods=['GET']) 
def ma_fonction(id): 
  print(id)
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute("select * from chambre where id="+id)
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data)
  else:
    return json.dumps({'error':str(data[0])})
  conn.close()
    
@app.route('/chambres', methods=['GET']) 
def getchambres(): 
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute("select * from chambre")
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data),200
  else:
    return json.dumps({'error':str(data[0])})
  conn.close()
    
@app.route('/chambresparhotel', methods=['GET']) 
def getchambresparhotel(): 
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute("SELECT idCategorie,count(idCategorie) FROM chambre as cha WHERE NOT EXISTS (SELECT * FROM reservation inner join chambre on reservation.idChambre=chambre.id where cha.id=reservation.idChambre and strftime('%s', '"+datetime.today().strftime('%Y-%m-%d')+"') >= strftime('%s', dateD) and strftime('%s', '"+datetime.today().strftime('%Y-%m-%d')+"') < strftime('%s', dateF))GROUP BY idCategorie")
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data),200
  else:
    return json.dumps({'error':str(data[0])})
  conn.close()
    
@app.route('/clic', methods=['GET']) 
def getclic(): 
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute("select * from clic")
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data),200
  else:
    return json.dumps({'error':str(data[0])})
  conn.close()
    
@app.route('/chambressearch', methods=['GET']) 
def getchambressearch():
  cat = str(request.args.get('cat'))
  dated = str(request.args.get('dated'))
  datef = str(request.args.get('datef'))
  reserv = str(request.args.get('reserv'))
  link=""
  requeststring="select * from chambre"
  if cat!="None" or dated!="None" or datef!="None" or reserv!="None":
    requeststring+=" WHERE "
    if cat!="None":
        requeststring+="idCategorie="+cat
        link=" AND "
        conn = sql.connect("sqlite-master/finder.db")
        curr = conn.cursor()
        curr.execute("UPDATE clic SET nbClics=nbClics+1 WHERE id="+cat)
        conn.commit()
        conn.close()
    if dated!="None" and datef!="None":
        link2="not "
        if reserv=="true":
            link2=""
        requeststring+=link+"chambre.id "+link2+"in (select reservation.idChambre from reservation inner join chambre on reservation.idChambre=chambre.id where strftime('%s', '"+dated+"') >= strftime('%s', dateD) and strftime('%s', '"+dated+"') < strftime('%s', dateF) or strftime('%s', '"+datef+"') >= strftime('%s', dateD) and strftime('%s', '"+datef+"') < strftime('%s', dateF))"
        link=" AND "
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute(requeststring)
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data),200
  else:
    return jsonify(data),200
  conn.close()
    
@app.route('/user', methods=['GET']) 
def userSelect(): 
 #conn = mysql.connect()#cursor = conn.cursor() #cursor.execute('select * from gateau')
  email = request.values.get('email')
  mdp = request.values.get('mdp')
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute("select * from user where email='"+email+"' and mdp='"+mdp+"'")
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) != 0:
    return jsonify(data), 200
  else:
    return jsonify(data), 401
  conn.close()
    
@app.route('/user', methods=['POST']) 
def postuser(): 
  mail = request.values.get('mail')
  nom = request.values.get('nom')
  prenom = request.values.get('prenom')
  mdp = request.values.get('mdp')
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  try:
    cur.execute("INSERT INTO user(email, nom, prenom, mdp) VALUES ('"+mail+"', '"+nom+"', '"+prenom+"', '"+mdp+"')")
    con.commit()
    msg = "Record successfully added"
    return jsonify(msg), 200
  except:
    con.rollback()
    msg = "error in insert operation"
    return jsonify(msg), 401

  finally:         
    con.close()
    
@app.route('/user', methods=['DELETE']) 
def deleteuser(): 
  #conn = mysql.connect()#cursor = conn.cursor() #cursor.execute('select * from gateau')
  nom = request.values.get('nom')
  prenom = request.values.get('prenom')
  token = request.values.get('token')

  try:
      jwt.decode(token, "secret", algorithms='HS256')
      con = sql.connect("sqlite-master/finder.db")
      con.row_factory = sql.Row
      cur = con.cursor()
      try:
        cur.execute("DELETE FROM user WHERE nom='"+nom+"' AND prenom='"+prenom+"'")
        con.commit()
        msg = "Record successfully added"
        return jsonify(msg), 200
      except:
        con.rollback()
        msg = "error in insert operation"
        return jsonify(msg), 401

      finally:
        con.close()
  except:
    return jsonify("error in insert operation"), 401

@app.route('/user', methods=['PUT']) 
def putuser(): 
  #conn = mysql.connect()#cursor = conn.cursor() #cursor.execute('select * from gateau')
  email = request.values.get('email')
  prenom = request.values.get('prenom')
  token = request.values.get('token')
  
  try:
      jwt.decode(token, "secret", algorithms='HS256')
      con = sql.connect("sqlite-master/finder.db")
      con.row_factory = sql.Row
      cur = con.cursor()
      try:
        cur.execute("UPDATE user SET email= '"+email+"' WHERE prenom='"+prenom+"'")
        con.commit()
        msg = "Record successfully added"
        return jsonify(msg), 200
      except:
        con.rollback()
        msg = "error in insert operation"
        return jsonify(msg), 401
      finally:
        con.close()
  except:
      return jsonify("error in insert operation"), 401

    
@app.route('/obtentionToken', methods=['GET']) 
def obtentionToken():
  #conn = mysql.connect()#cursor = conn.cursor() #cursor.execute('select * from gateau')
  login = request.values.get('login')
  password = request.values.get('password')
  
  if(ckeckUser(login,password)):
    token = jwt.encode({'loginpassword': str(login)+password}, "secret", algorithm='HS256')
    return jsonify(token), 200
  else:
    return jsonify("error"), 401
  
  
def ckeckUser(login, password): 
  #conn = mysql.connect()#cursor = conn.cursor() #cursor.execute('select * from gateau')
  
  con = sql.connect("sqlite-master/finder.db")
  con.row_factory = sql.Row
  cur = con.cursor()
  cur.execute("select * from user where email='"+login+"' and mdp='"+password+"'")
  data=[]
  rows = cur.fetchall(); 
  for row in rows:
    data.append(list(row))
  if len(data) == 1:
    return True
  else:
    return False
  conn.close()
 


if __name__ == '__main__':
    app.run(debug=True)