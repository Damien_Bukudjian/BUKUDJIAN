<?php
class plat
{
    private $id;
    private $nom;
    private $prix;
    private $image;
    private $quantite;
	private $marque;
	private $glacon;
	
    public function getNom()
    {
        return $this->nom;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getPrix()
    {
        return $this->prix;
    }
    public function getImage()
    {
        return $this->image;
    }
    public function getQuantite()
    {
        return $this->quantite;
    }
    public function getMarque()
    {
        return $this->marque;
    }
    public function getGlacon()
    {
        return $this->glacon;
    }
	
	public function setNom(string $nom) {
        $this->nom = $nom;
    }
	public function setId(string $id) {
        $this->id = $id;
    }
	public function setPrix(string $prix) {
        $this->prix = $prix;
    }
	public function setImage(string $image) {
        $this->image = $image;
    }
	public function setQuantite(string $quantite) {
        $this->quantite = $quantite;
    }
	public function setMarque(string $marque) {
        $this->marque = $marque;
    }
	public function setGlacon(string $glacon) {
        $this->glacon = $glacon;
    }
	
	public function __toString(){
      return $this->id." ".$this->prix." ".$this->quantite." ".$this->glacon; 
    }

} 
?>