<?php

interface ManagerInterface {
     function setConnection() ;
       function select(int $id);
      function insert( $obj): bool;
      function update( $obj): bool;
      function delete( $obj): bool;
      function findAll(): array;
}



 class DessertDAO implements ManagerInterface {
    private PDO $connection;
    
    
    public function setConnection() {
        $dsn='mysql:dbname=risto;host=127.0.0.1';
		$user='root';
		$password='d3d1a4b3';
    try{
        $dbh=new PDO($dsn,$user,$password); 
        $this->connection=$dbh;
    }catch(PDOException $e){
        echo'Connexion échouée:'.$e->getMessage(); 
    }
    }
  
    public function insert( $obj):bool {
        $result=false;
        try {
          /*$sql="SELECT max(id)+1 as id FROM personne";
            $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
            $row  = $statement -> fetch();
            $id=$row['id'];*/
            $id=$obj->getId();
            $nom=$obj->getNom();
			$prix=$obj->getPrix();
            $image=$obj->getImage();
			$sql="INSERT INTO desserts (Id, Nom, Prix, Image) VALUES ( '$id' , '$nom', '$prix', '$image')";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'insert échoué:'.$e->getMessage(); 
        }
        return $result;
    }
    public function select(int $id):Dessert {
         $p = new Dessert();
        try {
            $sql="SELECT * FROM dessert where Id = '$id'";
            $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
            $row  = $statement -> fetch();
            $p->setNom($row['Nom']);
            $p->setId($row['Id']);
			$p->setPrix($row['Prix']);
            $p->setImage($row['Image']);
        } catch (PDOException $e) {
            echo'select échoué:'.$e->getMessage(); 
        }
        return $p;
    }
public function update( $obj):bool {
        $result=false;
        try {
			$id=$obj->getId();
            $nom=$obj->getNom();
			$prix=$obj->getPrix();
            $image=$obj->getImage();
			$sql="UPDATE dessert SET Nom = '$nom', Prix = '$prix', Image = '$image'  WHERE Id = '$id'";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'update échoué:'.$e->getMessage(); 
        }
        return $result;
 }
public function delete( $obj):bool {
        $result=false;
        try {
            $id=$obj->getId();
          $sql="DELETE FROM dessert WHERE Id = '$id'";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'delete échoué:'.$e->getMessage(); 
        }
        return $result;
 }
//ici le tableau ne contient pas des objets de type Personne
   public function findAll():array {
        $dessert = null;
        try {
            $sql="SELECT * FROM dessert";
            $statement=$this->connection->query($sql);
            $dessert = $statement->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo'select all échoué:'.$e->getMessage(); 
        }
        return $dessert;
    }
}
echo "debut du programme";
$a=new Dessert();
$a->setNom("harry");
$a->setId(32);
var_dump($a);
$b=new Dessert();
$b->setNom("harold");
$b->setId(31);
$dessedao=new DessertDAO();
$dessedao->setConnection();
$dessedao->insert($a);
$dessedao->insert($b);
$a->setNom("jo");
$dessedao->update($a);
$unDessert=$dessedao->select(32);
echo "<br><br>".$unDessert."<br><br>";
$tb=$dessedao->findAll();
var_dump($tb);
foreach ($tb as $row ) {
  echo  $row->nom."<br><br>";
 }
 $tb[] = $b;
var_dump($tb);
$dessedao->delete($a);
/*CREATE TABLE IF NOT EXISTS `personne` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
)*/
?>