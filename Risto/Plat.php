<?php
class plat
{
    private $id;
    private $nom;
    private $prix;
    private $image;
    private $accompagnement;
	
    public function getNom()
    {
        return $this->nom;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getPrix()
    {
        return $this->prix;
    }
    public function getImage()
    {
        return $this->image;
    }
    public function getAccompagnement()
    {
        return $this->accompagnement;
    }
	public function setNom(string $nom) {
        $this->nom = $nom;
    }
	public function setId(string $id) {
        $this->id = $id;
    }
	public function setPrix(string $prix) {
        $this->prix = $prix;
    }
	public function setImage(string $image) {
        $this->image = $image;
    }
	public function setAccompagnement(string $accompagnement) {
        $this->accompagnement = $accompagnement;
    }

	public function __toString(){
      return $this->id." ".$this->prix." ";
    }
} 
?>