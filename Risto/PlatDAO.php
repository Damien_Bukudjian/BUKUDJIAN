<?php

interface ManagerInterface {
     function setConnection() ;
       function select(int $id);
      function insert( $obj): bool;
      function update( $obj): bool;
      function delete( $obj): bool;
      function findAll(): array;
}



 class PlatDAO implements ManagerInterface {
    private PDO $connection;
    
    
    public function setConnection() {
        $dsn='mysql:dbname=risto;host=127.0.0.1';
		$user='root';
		$password='d3d1a4b3';
    try{
        $dbh=new PDO($dsn,$user,$password); 
        $this->connection=$dbh;
    }catch(PDOException $e){
        echo'Connexion échouée:'.$e->getMessage(); 
    }
    }
  
    public function insert( $obj):bool {
        $result=false;
        try {
          /*$sql="SELECT max(id)+1 as id FROM personne";
            $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
            $row  = $statement -> fetch();
            $id=$row['id'];*/
            $id=$obj->getId();
            $nom=$obj->getNom();
			$prix=$obj->getPrix();
            $image=$obj->getImage();
			$accompagnement=$obj->getAccompagnement();
			$sql="INSERT INTO plats (Id, Nom, Prix, Image, Accompagnement) VALUES ( '$id' , '$nom', '$prix', '$image', '$accompagnement')";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'insert échoué:'.$e->getMessage(); 
        }
        return $result;
    }
    public function select(int $id):Plat {
         $p = new Plat();
        try {
            $sql="SELECT * FROM plats where Id = '$id'";
            $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
            $row  = $statement -> fetch();
            $p->setNom($row['Nom']);
            $p->setId($row['Id']);
			$p->setPrix($row['Prix']);
            $p->setImage($row['Image']);
			$p->setAccompagnement($row['Accompagnement']);
        } catch (PDOException $e) {
            echo'select échoué:'.$e->getMessage(); 
        }
        return $p;
    }
public function update( $obj):bool {
        $result=false;
        try {
			$id=$obj->getId();
            $nom=$obj->getNom();
			$prix=$obj->getPrix();
            $image=$obj->getImage();
			$accompagnement=$obj->getAccompagnement();
			$sql="UPDATE plats SET Nom = '$nom', Prix = '$prix', Image = '$image', Accompagnement = '$accompagnement'  WHERE Id = '$id'";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'update échoué:'.$e->getMessage(); 
        }
        return $result;
 }
public function delete( $obj):bool {
        $result=false;
        try {
            $id=$obj->getId();
          $sql="DELETE FROM plats WHERE Id = '$id'";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'delete échoué:'.$e->getMessage(); 
        }
        return $result;
 }
//ici le tableau ne contient pas des objets de type Personne
   public function findAll():array {
        $plat = null;
        try {
            $sql="SELECT * FROM plats";
            $statement=$this->connection->query($sql);
            $plat = $statement->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo'select all échoué:'.$e->getMessage(); 
        }
        return $plat;
    }
}
echo "debut du programme";
$a=new Plat();
$a->setNom("harry");
$a->setId(32);
var_dump($a);
$b=new Plat();
$b->setNom("harold");
$b->setId(31);
$pladao=new PlatDAO();
$pladao->setConnection();
$pladao->insert($a);
$pladao->insert($b);
$a->setNom("jo");
$pladao->update($a);
$unPlat=$pladao->select(32);
echo "<br><br>".$unPlat."<br><br>";
$tb=$pladao->findAll();
var_dump($tb);
foreach ($tb as $row ) {
  echo  $row->nom."<br><br>";
 }
 $tb[] = $b;
var_dump($tb);
$pladao->delete($a);
/*CREATE TABLE IF NOT EXISTS `personne` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
)*/
?>