# Configuration de ton PC Gamer !

![mainimg](https://images-na.ssl-images-amazon.com/images/I/515HEiDXa4L.jpg)

## Voici les composants :

* Processeur : le **moteur** de ton ordinateur ! C'est un AMD Athlon 64 FX 3,4Ghz (la vitesse du processeur), il te permettra d'avoir de bonnes performances tout en étant peu coûteux.

* Carte graphique : elle va te permettre d'afficher tout ce que tu vois à l'écran avec une 
certaine qualité. C'est une Nvidia GeForce 1050 Ti ! La marque Nvidia est une marque fiable et 
connue pour sa haute gamme en terme de jeux vidéo. La 1050 Ti est une entrée de gamme pour les gamers.

* La RAM : 8Go, c'est suffisant pour l'utilisation que tu auras de ton pc (jeux vidéos).

* Disque dur : 1To te permettras de stocker beaucoup de jeux sur ton ordinateur sans devoir en désinstaller sans arrêt. C'est un HDD, moins rapide qu'un SSD mais il sera suffisant pour les jeux auxquels tu souhaites jouer. Cependant, si tu télécharges des jeux plus lourds (plus difficiles à faire tourner), ils mettront un peu plus de temps à se lancer mais ça ne sera pas gênant.

* Ecran : 22", parfait pour avoir une bonne vision d'ensemble et un certain confort !

Avec cette configuration, tu pourras jouer à Minecraft, Fortnite et plein d'autres jeux de manière fluide (environs 50fps), avec une qualité plus que suffisante (et agréable). Ton pc est aussi accompagné d'un clavier et d'une souris que tu pourras renvendre, ainsi que d'un casque pour optimiser tes parties sans être dérangée par les bruits environnants.

# Le tout pour 700€ !

Voici le lien de la configuration : https://urlz.fr/aCZc