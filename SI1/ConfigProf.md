# Configuration de votre PC Portable

![image](https://images-na.ssl-images-amazon.com/images/I/919owpRJ3CL._SL1500_.jpg)


* Processeur: AMD Ryzen 1800x

* Carte graphique: AMD  Radeon Vega 3, elle sera suffisante pour une utilisation basique (internet, logiciels, vidéos).

* Ram: 4Go

* Disque dur: 1To, plus que suffisant et vous pourrez stocker toutes sortes de logiciels plus ou moins lourds selon le contenu de vos cours. 

* Ecran: 17,3", plus que votre demande pour vous assurer un meilleur confort que prévu !

# Prix total : 499€ !

Lien du pc : https://urlz.fr/aCZM