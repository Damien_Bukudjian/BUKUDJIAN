# Etape 1

* Version 6.0
* Windows 10
* Redemarrer le PC et appuyer sur F10
* Virtualisation logiciel

# Etape 2

* Debian 10.1
* Crée une machine virtuel sur virtualbox, allocation de memoire et disque dur, y ajouter l'ISO de Debian, puis installateur Debian (Trop tard pour les screens)
* damien
* Environnement de bureau Debian, GNOME, serveur d'impression, serveur SSH, utilitaires usuels du système

# Etape 3
* WIFI ITESCIA
* La VM recois de la connexion grâce à la carte réseau du PC hôte en passant par le NAT.
* ping google.com, cela marche.
* Oui
* La commande sudo dhclient permet de verifier.

# Etape 4

* Installer les additions invités, puis aller dans Configuration/Dossier partagé puis ajouter un nouveau dossier. Ajouter ensuite le chemin jusqu'au dossier à partager dans la VM. Cocher "Configuration permanente". Dans "point de montage" choisir le chemin vers le dossier sur la VM. Executer ensuite la commande suivante: sudo mount -t vboxsf dossierhote /chemindossiervm
* Fait
* Si on à fait le dossier par la ligne de commande il faut refaire la commande à chaques demarrage "sudo mount -t vboxsf dossierhote /chemindossiervm". Il faut cocher "Montage automatique" pour eviter de refaire cette commande à chaque démarrage. Le point de montage se recrée automatiquement à chaque démarrage.

# Etape 5
* Il faut mettre l'acces réseau en "acces par pont" dans l'inteface VirtualBox. 
* Pour verifier la connexion on effectue un ping entre les 2 machines, si les paquets sont bien reçu, les machines peuvent discuter

![image](https://zupimages.net/up/19/49/lta1.png)