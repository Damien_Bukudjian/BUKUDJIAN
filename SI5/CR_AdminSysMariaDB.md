Activité 3 : Installation BDD

Damien Bukudjian


Fait en collaboration avec Allyson Hubert


# Etape 1

* sudo apt-get install mariadb-server puis sudo mysql_secure_installation
* systemctl start mariadb pour demarrer le serveur, systemctl stop mariadb pour l'arreter et systemctl restart mariadb pour le redemarrer.
* sudo mysql -u root -p pour se connecter au serveur. On peut ensuite utiliser la commande "USE mysql" pour se connecter à la base de donnée "mysql". On peut ensuite utiliser les commande "SELECT" pour afficher les données ou "UPDATE" pour modifier des données.
* /etc/mysql/mariadb.cnf et /etc/mysql/mariadb.conf.d et /etc/mysql/conf.d

# Etape 2

* Il faut installer libapache2-mod-php.
* apt install libapache2-mod-php
* mv test.html test.php, echo "<?php phpinfo() ; ?>" >> test.php 
* La machine cliente et serveur est la machine local (localhost)


# Etape 3

* cd /var/www/html. Puis sudo wget https://www.adminer.org/en/#download
* mv index.html adminer.php
* La machine cliente et serveur est la machine local (localhost)

# Etape 4

* Il y a NAT, réseau intere et accès par pont. Ici c'est un réseau interne.
* On utilise la commande ping suivi de l'adresse ip de la machine avec laquelle on veux communiquer.
* Réseau non identifié, pas d'accès à internet.
* Adresse introuvable lorsque l'on veux acceder à internet. Il faut utiliser le mode d'accès réseau "Pilote générique"

# Etape 5

![captureact3](Images\act3.png)
