Activité 2 : Wireshark

# Etape 1

* Ce logiciel permet les écoutes du traffic sur le réseau.


# Etape 2

* Le protocole HTTP permet de communiquer entre le client et le serveur. Il appartient à la couche session. Le navigateur permet de faire des requêtes, le moteur de recherche y repond.
* En requête: L'URL, la version du protocole, les en-tête et le corps de la requêtes sont envoyé et en réponse: le statut, l'en-tête de la réponse et le corps.
* Le navigateur. Il est installé sur ma machine, sous windows. L'adresse IP est 192.168.0.31/24
* Wampserver sous Apache
![captureq3](Images/q3.png)
![Capture](Images/Capture.png)

# Etape 3

* Il permet, depuis un ordinateur, de copier des fichiers vers un autre ordinateur du réseau, ou encore de supprimer ou de modifier des fichiers sur cet ordinateur. Il appartient à la couche application. FileZilla par exemple le permet depuis le client et FileZilla Server depis le serveur
* Le port source, le port destination, les données
* Client FTP : FileZilla Server sur Mac ou Windows (machine hôte)
* Adresse IP : 192.168.0.27. Port 21
![captureq6](Images/q6.png)

# Etape 4

* SSH est un protocole réseau qui permet aux administrateurs d'accéder à distance à un ordinateur, en toute sécurité. Il appartient à la couche application. On peux utiliser Putti pour le client et Apache pour le serveur.
* 
* Putti sur ma machine. Windows 10
* OpenSSH sur la VM. Sous Debian.
* 
* 