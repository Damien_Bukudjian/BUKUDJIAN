import java.util.ArrayList;

public class Hearthstone {
    private int idcl;
    private String libellecl;

    public Hearthstone() {
    }

    public Hearthstone(int idcl, String libellecl) {
        this.libellecl = libellecl;
        this.idcl = idcl;
    }

    public int getIdcl() {
        return idcl;
    }
    public void setIdcl(int idcl) { this.idcl = idcl; }

    public String getLibellecl() {
        return libellecl;
    }
    public void setLibellecl(String url) { this.libellecl = url; }

    public String toString(){
        return "id : " + this.idcl + " | libelle : " + libellecl;
    }
}
