import java.sql.*;
import java.util.ArrayList;

public class HearthstoneDAO {
    private static Connection conn = null;
    private static String url = "";
    private static String user = "";
    private static String password = "";

    public HearthstoneDAO(){
    }

    public HearthstoneDAO(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString(){
        return this.url + " " + this.user + " " + this.password;
    }

    public void createConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn= DriverManager.getConnection(url,user, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Hearthstone> requeteSelect(String req) throws SQLException {
        ResultSet res = null;
        ArrayList list = new ArrayList();
        try {
            Statement stmt = conn.createStatement();
            res = stmt.executeQuery(req);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        Hearthstone classes;
        while(res.next()){
            classes = new Hearthstone(res.getInt("idcl"),res.getString("libellecl"));
            list.add(classes);
        }
        return list;
    }

    public void requeteUpdate(String req){
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            int res2 = stmt.executeUpdate(req);
            System.out.println("nb de modifications réalisées : " + res2);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}