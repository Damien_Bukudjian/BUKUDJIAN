//Damien et Alex

import java.sql.SQLException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws SQLException {
        /*
        Exercice 1
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn= DriverManager.getConnection("jdbc:mysql://localhost/hearthstone?serverTimezone=UTC&useLegacyDatetimeCode=false","admin", "admin");
            Statement stmt = conn.createStatement();
            //Etape 1
            String req1 = "SELECT * FROM classes";
            requeteSelect(stmt, req1);
            String req2 = "INSERT INTO classes VALUES (1, 'abcd')";
            int res2 = stmt.executeUpdate(req2);
            System.out.println("nb de modifications réalisées : " + res2);
            requeteSelect(stmt, req1);
            String req3 = "DELETE FROM classes WHERE idcl=1";
            int res3 = stmt.executeUpdate(req3);
            System.out.println("nb de modifications réalisées : " + res3);
            requeteSelect(stmt, req1);
            //Etape 2

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/

        String url = "jdbc:mysql://localhost/hearthstone?serverTimezone=UTC&useLegacyDatetimeCode=false";
        String user = "root";
        String password = "";
        HearthstoneDAO maConnexionDAO = new HearthstoneDAO(url, user, password);
        //maConnexion.setUrl(url);
        //maConnexion.setUser(user);
        //maConnexion.setPassword(password);
        maConnexionDAO.createConnection();

        String req1 = "SELECT * FROM classes";
        ArrayList<Hearthstone> list = maConnexionDAO.requeteSelect(req1);

        //print list values
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i).toString());
        }

        String req2 = "INSERT INTO classes VALUES (1, 'abcd')";
        maConnexionDAO.requeteUpdate(req2);

        list = maConnexionDAO.requeteSelect(req1);
        //print list values
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i).toString());
        }

        String req3 = "DELETE FROM classes WHERE idcl=1";
        maConnexionDAO.requeteUpdate(req3);
        list = maConnexionDAO.requeteSelect(req1);
        //print list values
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i).toString());
        }
    }

    /*    public void requeteSelect(Statement stmt, String req1){
        ResultSet res = null;
        try {
            res = stmt.executeQuery(req1);

        while(res.next()){
            //Retrieve by column name
            int idcl  = res.getInt("idcl");
            String libellecl = res.getString("libellecl");

            //Display values
            System.out.print("ID: " + idcl);
            System.out.println(", libellecl: " + libellecl);
        }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }*/
}
