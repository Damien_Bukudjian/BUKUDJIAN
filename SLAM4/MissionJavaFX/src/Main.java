import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;

public class Main extends Application  {
    private Stage stage;
    Label result = new Label("");
    @Override
    public void start(Stage primaryStage) {
        VBox vBox = new VBox();

        vBox.setSpacing(8);
        vBox.setPadding(new Insets(10,10,10,10));
        Button search = new Button("Chercher !");
        TextField input = new TextField();
        vBox.getChildren().addAll(
                new Label("Identifiant :"),
                input,
                search,
                result);
        vBox.setStyle("-fx-background-color: #FFFFFF;");
        vBox.setStyle("-fx-background-radius: 10 10 10 10;");

        Scene scene = new Scene(vBox,400,400);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("Mission JavaFX");
        primaryStage.setAlwaysOnTop(true);

        search.setOnAction(e -> {
            if (input.getText().length()>0) {
                try {
                    search(Integer.parseInt(input.getText()));
                }
                catch(Exception ex){
                    result.setText("Veuillez entrer un nombre.");
                }
            }
            else{
                result.setText("Veuillez entrer quelque chose.");
            }
        });

    }

    private void search(int id) {
        String url = "jdbc:mysql://localhost/hearthstone?serverTimezone=UTC&useLegacyDatetimeCode=false";
        String user = "root";
        String password = "";

        try {
            HearthstoneDAO maConnexionDAO = new HearthstoneDAO(url, user, password);
            maConnexionDAO.createConnection();
            String req = "SELECT * FROM enseignants WHERE id="+id;
            ArrayList userData = maConnexionDAO.requeteSelect(req);
            //print list values
            if (userData.size()>0){
                for(int i=0;i<userData.size();i++){
                    result.setText(userData.get(i).toString()+"\n");
                }
            }
            else{
                result.setText("Pas de résultat.");
            }


        } catch (SQLSyntaxErrorException | ClassNotFoundException thowable) {
            result.setText("Impossible d'accéder à la base.\n"+thowable);
        }
        catch (SQLException throwables) {
            result.setText("Erreur de requête à la base.\n"+throwables);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}