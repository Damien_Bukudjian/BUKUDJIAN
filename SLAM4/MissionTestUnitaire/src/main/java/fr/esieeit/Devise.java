package fr.esieeit;

public class Devise {

    private int quantite;
    private String monnaie;

    public Devise(int somme, String monnaie) {
        this.quantite = somme;
        this.monnaie = monnaie;
    }
    public int getQuantite() {
        return quantite;
    }
    public String getMonnaie() {
        return monnaie;
    }
    public Devise add(Devise m) throws MonnaieDifferenteException{
        if(this.getMonnaie() == m.getMonnaie()){
            return new Devise(getQuantite()+m.getQuantite(), getMonnaie());
        }
        throw (new MonnaieDifferenteException(this,m));

    }

    public String toString(){
        return this.getQuantite()+this.getMonnaie();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Devise) {
            Devise objAComparer = Devise.class.cast(obj);
            if(this.monnaie == objAComparer.getMonnaie()){
                if(this.quantite == objAComparer.getQuantite()){
                    return true;
                }
            }
        }
        return false;
    }
}

