package fr.esieeit;
import java.util.HashMap;

public class Portefeuille {
    private HashMap<String, Devise> contenu;
    public HashMap<String, Devise> getContenu() {
        return contenu;
    }
    public Portefeuille() {
        contenu = new HashMap<String, Devise>();
    }
    public void ajouteDevise(Devise d) {
        boolean test = false;
        for (HashMap.Entry<String, Devise> item : contenu.entrySet()) {
            if(item.getValue().getMonnaie().equals(d.getMonnaie())){
                test = true;
                contenu.put(item.getKey(),new Devise(item.getValue().getQuantite()+d.getQuantite(),item.getValue().getMonnaie()));
            }
        }
        if(!test){
            contenu.put(d.getMonnaie(),d);
        }
    }

    public String toString(){
        String result="";
        for (HashMap.Entry<String, Devise> item : contenu.entrySet()) {
            result+= item.getValue().getQuantite() + item.getValue().getMonnaie()+"\n";
        }
        return result;
    }
}