package fr.esieeit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeviseTest {
    Devise devise112 = new Devise(12, "EUR");
    Devise devise120 = new Devise(20, "EUR");
    Devise devise220 = new Devise(20, "CHF");
    Devise devise210 = new Devise(10, "CHF");
    static int compteur=0;

    @Test
    public void testEquals() {
        Assert.assertEquals(devise112.equals(devise112),true);
        Assert.assertEquals(devise112.equals(devise120),false);
        Assert.assertEquals(devise120.equals(devise220),false);
    }

    @Test
    public void testAdd() throws MonnaieDifferenteException {
        Devise expected = new Devise(30, "CHF");
        Devise result = devise210.add(devise220);
        Assert.assertEquals(expected, result);
    }

    @Test(expected = MonnaieDifferenteException.class)
    public void testDeviseFormat() throws MonnaieDifferenteException {
        devise112.add(devise220);
    }

    @Before
    public void methodeBefore() {
        compteur++;
        System.out.println(compteur+"e passage avant execution d'une methode de test"); }
    @After
    public void methodeAfter() { System.out.println(compteur+"e passage apres execution d'une methode de test");  }

}