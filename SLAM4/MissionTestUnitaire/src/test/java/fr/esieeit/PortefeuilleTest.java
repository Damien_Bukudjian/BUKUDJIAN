package fr.esieeit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class PortefeuilleTest {
    Portefeuille portefeuille = new Portefeuille();
    Portefeuille portefeuille2 = new Portefeuille();
    static int compteur=0;

    @Test
    public void testAjouteDevise() {
        portefeuille.ajouteDevise(new Devise(10,"EUR"));
        portefeuille.ajouteDevise(new Devise(10,"EUR"));
        portefeuille.ajouteDevise(new Devise(5,"CHF"));
        portefeuille.ajouteDevise(new Devise(15,"CHF"));

        portefeuille2.ajouteDevise(new Devise(20,"CHF"));
        Assert.assertNotEquals(portefeuille.getContenu().get("EUR"),portefeuille.getContenu().get("CHF"));
        Assert.assertEquals(portefeuille.getContenu().get("CHF"),portefeuille2.getContenu().get("CHF"));
    }

    @Before
    public void methodeBefore() {
        compteur++;
        System.out.println(compteur+"e passage avant execution d'une methode de test"); }
    @After
    public void methodeAfter() { System.out.println(compteur+"e passage apres execution d'une methode de test");  }
}