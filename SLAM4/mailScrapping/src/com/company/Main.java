package com.company;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//comment the above line and uncomment below line to use Chrome
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<String> url = new ArrayList<String>();

        // declaration and instantiation of objects/variables
        System.setProperty("webdriver.chrome.driver", "D:\\ITESCIA-GIT\\BUKUDJIAN\\SLAM4\\mailScrapping\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //comment the above 2 lines and uncomment below 2 lines to use Chrome
        //System.setProperty("webdriver.chrome.driver","G:\\chromedriver.exe");
        //WebDriver driver = new ChromeDriver();

        for(int i=1;i<=100;i++) {
            String departement="Ile-de-France";
            int page=i;
            String secteur="Gestion";
            String baseUrl = "https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=gestion&ou="+departement+"&idOu=R11&page="+page+"&contexte=Q93zd2bNQ2EyQJGGxM2baXeKL16bkxx3e0d5jKAkSaA%3D&proximite=0&quoiQuiInterprete="+secteur;

            // launch Fire fox and direct it to the Base URL
            driver.get(baseUrl);

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //driver.findElement(By.xpath("//*[contains(text(), 'Accepter & fermer')]")).click();


            List<WebElement> entreprises = driver.findElements(By.xpath("//*[contains(@title, 'Accéder au site Internet')]"));

            for (WebElement we : entreprises) {
                we.click();
            }
            ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());

            ArrayList<String> tempUrl = new ArrayList<String>();
            Boolean check = false;

            for (String actual : tabs2) {
                driver.switchTo().window(actual); //opening the URL saved.
                if(!driver.getCurrentUrl().contains("pagesjaunes")) {
                    if(!url.contains(driver.getCurrentUrl())) {
                        url.add(driver.getCurrentUrl());
                        tempUrl.add(driver.getCurrentUrl());
                        check = true;
                    }
                    driver.close();
                }
            }
            if(check){
                String finalstr = "";
                for(String element : tempUrl){
                    finalstr+=element+"\n";
                }

                FileWriter fw = new FileWriter("D:\\ITESCIA-GIT\\BUKUDJIAN\\SLAM4\\mailScrapping\\urls.txt",true);
                fw.write(finalstr);
                fw.close();
            }
            driver.switchTo().window(tabs2.get(0));
        }
    }

}