<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ChambreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chambre= Chambre::create([
            'nbCouchage' =>2,
            'porte' => 'B',
            'etage' => 10,
            'idCategorie' => 1,
            'baignoire' => 0,
            'prixBase' => 50
        ]);
        //affichage en console
        dd($chambre);
    }
}
