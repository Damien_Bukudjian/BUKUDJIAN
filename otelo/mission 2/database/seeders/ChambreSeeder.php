<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

 use App\Models\Chambre;

class ChambreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<29;$i++){
			$nb = random_int(1,5);
			$porte = chr(random_int(65,69));
			$etage = random_int(1,3);
			$idCate = random_int(1,5);
			$baignoire = random_int(0,2);
			$prix = random_int(50,150);

			$chambre= Chambre::create([
				'nbCouchage' =>$nb,
				'porte' => $porte,
				'etage' => $etage,
				'idCategorie' => $idCate,
				'baignoire' => $baignoire,
				'prixBase' => $prix
			]);
        }
    }
}
