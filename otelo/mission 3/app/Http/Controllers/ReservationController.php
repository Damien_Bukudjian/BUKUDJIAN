<?php 
namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use App\Models\Reservation;

class ReservationController extends Controller{ 
   public function create(){ 
     return view('createReservation');
}
   public function store(Request $request){ 
		$validatedData = $request->validate([
			'dated' => 'required|date|after:tomorrow',
			'datef' => 'required|date|after:dated',
			'per'=> 'required|between:1,3'
        ]);
        $reservation=new Reservation();
        $dateD=$request->input('dated');
        $dateF=$request->input('datef');
        $idPeriode=$request->input('per');
        $date=strtotime("now");
        $reservation->dateD=$dateD;
        $reservation->dateF=$dateF;
        $reservation->idPeriode=$idPeriode;
        $reservation->created_at=$date;
        $reservation->updated_at=$date;
        $reservation->save();
        return redirect()->back();
	}
}