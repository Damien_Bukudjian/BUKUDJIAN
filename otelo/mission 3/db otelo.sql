-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1,	'standard'),
(2,	'confort'),
(3,	'premium'),
(4,	'luxe');

DROP TABLE IF EXISTS `chambre`;
CREATE TABLE `chambre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nbCouchage` int(11) NOT NULL,
  `porte` varchar(5) NOT NULL,
  `etage` varchar(5) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `baignoire` tinyint(1) NOT NULL,
  `prixBase` double NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategorie` (`idCategorie`),
  CONSTRAINT `chambre_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categorie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `chambre` (`id`, `nbCouchage`, `porte`, `etage`, `idCategorie`, `baignoire`, `prixBase`, `updated_at`, `created_at`) VALUES
(115,	5,	'D',	'2',	3,	0,	103,	'2020-10-22',	'2020-10-22'),
(116,	5,	'E',	'1',	2,	1,	59,	'2020-10-22',	'2020-10-22'),
(117,	1,	'C',	'2',	2,	2,	110,	'2020-10-22',	'2020-10-22'),
(118,	2,	'A',	'3',	3,	0,	125,	'2020-10-22',	'2020-10-22'),
(119,	1,	'D',	'1',	2,	1,	123,	'2020-10-22',	'2020-10-22'),
(120,	4,	'E',	'3',	1,	1,	53,	'2020-10-22',	'2020-10-22'),
(121,	2,	'D',	'1',	2,	0,	88,	'2020-10-22',	'2020-10-22'),
(122,	1,	'B',	'2',	3,	0,	62,	'2020-10-22',	'2020-10-22'),
(123,	5,	'D',	'3',	1,	1,	110,	'2020-10-22',	'2020-10-22'),
(124,	1,	'C',	'1',	2,	2,	115,	'2020-10-22',	'2020-10-22'),
(125,	5,	'C',	'1',	1,	1,	134,	'2020-10-22',	'2020-10-22'),
(126,	3,	'E',	'1',	1,	2,	87,	'2020-10-22',	'2020-10-22'),
(127,	2,	'B',	'2',	2,	0,	129,	'2020-10-22',	'2020-10-22'),
(128,	5,	'C',	'2',	3,	2,	86,	'2020-10-22',	'2020-10-22'),
(129,	5,	'A',	'2',	3,	1,	112,	'2020-10-22',	'2020-10-22'),
(130,	3,	'A',	'3',	3,	1,	126,	'2020-10-22',	'2020-10-22'),
(131,	5,	'A',	'2',	1,	1,	136,	'2020-10-22',	'2020-10-22'),
(132,	4,	'E',	'3',	1,	1,	62,	'2020-10-22',	'2020-10-22'),
(133,	4,	'B',	'1',	2,	0,	110,	'2020-10-22',	'2020-10-22'),
(134,	2,	'D',	'3',	3,	1,	150,	'2020-10-22',	'2020-10-22'),
(135,	1,	'E',	'3',	2,	0,	91,	'2020-10-22',	'2020-10-22'),
(136,	5,	'E',	'2',	1,	2,	125,	'2020-10-22',	'2020-10-22'),
(137,	5,	'E',	'2',	3,	2,	148,	'2020-10-22',	'2020-10-22'),
(138,	1,	'D',	'3',	3,	2,	132,	'2020-10-22',	'2020-10-22'),
(139,	2,	'A',	'1',	2,	1,	118,	'2020-10-22',	'2020-10-22'),
(140,	5,	'E',	'3',	2,	0,	93,	'2020-10-22',	'2020-10-22'),
(141,	4,	'A',	'3',	1,	1,	79,	'2020-10-22',	'2020-10-22'),
(142,	3,	'D',	'3',	2,	1,	71,	'2020-10-22',	'2020-10-22'),
(143,	5,	'E',	'1',	2,	1,	113,	'2020-10-22',	'2020-10-22'),
(144,	2,	'E',	'2',	3,	2,	92,	'2020-10-22',	'2020-10-22');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2019_08_19_000000_create_failed_jobs_table',	1),
(4,	'2020_10_22_073936_create_reservation_table',	2);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `periode`;
CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `periode` (`id`, `libelle`, `coefficient`) VALUES
(1,	'basse',	0.7),
(2,	'moyenne',	1),
(3,	'haute',	2);

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dateD` date NOT NULL,
  `dateF` date NOT NULL,
  `idPeriode` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2020-11-17 08:18:03
