@extends('layouts.app')

@section('content')
<div class="container">
<form method="post" action="{{ route('reservation.store')}}">
@csrf
  <div class="form-group">
    <label for="dated">Date de début</label>
    <input name="dated" type="text" class="form-control" id="dated" placeholder="yyyy-mm-dd" required>
	@error('dated')
        <div class="alert alert-danger mt-2">
        {{$message}} Erreur : mauvaise date
        </div>
        @enderror
  </div>
  
  <div class="form-group">
    <label for="datef">Date de fin</label>
    <input name="datef" type="text" class="form-control" id="datef" placeholder="yyyy-mm-dd" required>
	@error('datef')
        <div class="alert alert-danger mt-2">
        {{$message}}  Erreur : mauvaise date
        </div>
        @enderror
  </div>
  
  <label>Période</label>
		<select name="per" class="form-control" id="per" style=" width: auto">
		  <option value="1">basse</option>
		  <option value="2">moyenne</option>
		  <option value="3">haute</option>
		</select>
		
	@error('per')
        <div class="alert alert-danger mt-2">
        {{$message}} Erreur : période
        </div>
        @enderror
  <button type="submit" class="btn btn-primary">Chercher !</button>
</form>
</div>
@stop
