<?php

namespace Database\Factories;

use App\Models\Chambre;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ChambreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Chambre::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $collection = collect(["A","B","C"]);
        return [
                'nbCouchage' => $n = rand(0,11),
                'porte' => $collection->random(),
                'etage' =>  $n = rand(0,11),
                'idCategorie' => $n = rand(1,4),
                'baignoire' => $n = rand(0,1),
                'prixBase' => $n = rand(50,4500)
        ];
        
    }
}
