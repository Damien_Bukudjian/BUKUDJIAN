

@extends('layouts.app')

@section('content')



<div style="margin-left: 42%;" class="container">
@if($errors->any())
 <div class='alert alert-danger'>
   <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach    
   </ul>
@endif
<form method="post" action="{{ route('reservation.store')}}">
@csrf


  
  <div class="form-group">
    <label  for="exampleInputEmail1">Date début</label>
    <input name="dateD" style="width: 15%;" type="date" class="form-control @error('dated') is-invalid @enderror"  id="exampleInputEmail1"  >
    @error('dateD')
    <div class="alert alert-danger mt-2">
        {{$message}} Il y'a une erreur
        </div>
        @enderror
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Date de fin</label>
    <input name="dateF" style="width: 15%;" type="date" class="form-control" id="exampleInputPassword1" >
  </div>
 

<div class="form-group">
  <label for="sel1">Select list:</label>
  <select style="width: 15%;" name="idPeriode" class="form-control" id="sel1">
    <option value=1>basse</option>
    <option value=2>moyenne</option>
    <option value=3>haute</option>
  </select>
  </div>
  <button type="submit" class="btn btn-primary">Réservation</button>
</form>
</div>



@stop