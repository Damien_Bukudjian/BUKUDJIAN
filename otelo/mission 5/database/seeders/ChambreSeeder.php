<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Chambre;



class ChambreSeeder extends Seeder
{
    
      



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collection = collect(["A","B","C"]);
        $chambre= Chambre::create([
            'nbCouchage' => $n = rand(0,11),
            'porte' => $collection->random,
            'etage' =>  $n = rand(0,11),
            'idCategorie' => $n = rand(1,4),
            'baignoire' => $n = rand(0,1),
            'prixBase' => $n = rand(50,4500),
        ]);
        //affichage en console
        dd($chambre);
    }

}
