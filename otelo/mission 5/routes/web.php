<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/accueil', function () {
    return view('accueil');
});

Route::get('/monUri','PremierController@home' );

Route::get('/chambre', 'App\Http\Controllers\ChambreController@store');

Route::get( '/newreservation','App\Http\Controllers\ReservationController@create')->middleware('auth');
Route::get('/failure',function () {
    return view('failure');
})->name('failure');

Route::post('/storereservation','App\Http\Controllers\ReservationController@store' )->name('reservation.store');

Route::get('/compte',function () {
     return view('auth.compte');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::get('/deconnexion',function () {
    Auth::logout();
     return redirect('/');
 });

 Route::get('/chambres','App\Http\Controllers\ChambreController@index');

 Route::get('/chambresPremium', 'App\Http\Controllers\ChambreController@premium');





 ?>

